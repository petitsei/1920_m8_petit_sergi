= Pràctica servidor FTP

:encoding: utf-8
:lang: ca
:toc: left
:!numbered:

<<<

== Apartat 1: Preparació de la màquina *Tyr*
====
image::images/1.png[]
====
*Captura del contingut del fitxer `/etc/netplan/xxx.yaml` de _tyr_.*
====
image::images/2.png[]
====
*Captura de la taula d'enrutament.*
====

== Apartat 2: Instal·lador del servidor FTP a _tyr_
====
image::images/3.png[]
====
*Captura de la transacció FTP entre _heimdall_ i _tyr_.*
====

== Apartat 3: Configuració de l'enrutament
====
image::images/4.png[]
====
*Captura de la transacció FTP entre l'ordinador amfitrió i _tyr_.*
====
image::images/5.png[]
====
*Captura de la configuració _iptables_ de _thor_.*
====
image::images/6.png[]
====
*Captura de la connexió al servei FTP des de _PC1_.*
====

== Apartat 4: Configuració de l'accés anònim
====
image::images/7.png[]
====
*Captura de la sortida de `ls -ld` sobre el nou directori.*
====
image::images/8.png[]
====
*Captura de les modificacions a la configuració.*
====
image::images/9.png[]
====
*Captura d'una transacció utilitzant l'accés anònim.*
====

== Apartat 5: Separació de la configuració del FTP en xarxes
====
image::images/10.png[]
image::images/11.png[]
image::images/12.png[]
====
*Canvis al fitxer de configuració*
====
image::images/13.png[]
====
*Fitxer /etc/hosts.allow*
====
image::images/15.png[]
====
*Comprovació accés exterior*
====
image::images/14.png[]
====
*Comprovació accés intern*
====

== Apartat 6: Configuració de l'espai compartit
====
image::images/16.png[]
image::images/17.png[]
====
*Ordre utilitzada per crear l'usuari _ftpuser_*
====
image::images/18.png[]
====
*Ordre utilitzada per donar una contrasenya a l'usuari _ftpuser_*
====
image::images/19.png[]
====
*Comprovació que l'usuari _ftpuser_ pot connectar-se al servei FTP.*
====
image::images/20.png[]
====
*Instrucció per reiniciar el servei vsftpd.*
====
image::images/21.png[]
====
*Instrucció per treure el permís d'escriptura al _home_ de l'usuari _ftpuser_.*
====
image::images/23.png[]
====
*Comprovació que _ftpuser_ i _anonymous_ poden connectar, però que el teu usuari
habitual no.*
====
== Apartat 7: Configuració de l'espai per a cada usuari
====
image::images/24.png[]
====
*Canvis fets a _/etc/vsftpd.lan.conf_.*
====
image::images/25.png[]
====
*Comprovació que un usuari pot pujar fitxers al servidor.*
====
== Apartat 8: Configuració de la capa de xifratge SSL
====
image::images/26.png[]
====
*Captura on es vegi l'usuari i contrasenya capturades pel _tcpdump_.*
====
image::images/27.png[]
image::images/28.png[]
image::images/29.png[]
====
*Captura on es vegi el Filezilla connectat a _tyr_.*
====